// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    css: [
        '@/assets/contents.css',
        '@/assets/layout.css'
    ],
    components : [
        {
            path:'~/components',
            extensions:['.vue'],
        }
    ]
})
